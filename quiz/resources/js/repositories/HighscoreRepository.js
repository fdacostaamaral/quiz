import Repository from 'axios';

const resource = '/highscores';

export default{
    get() {
        return Repository.get(`${resource}`);
    },

    addScore(payload){
        return Repository.post(`${resource}`, payload);
    }
}