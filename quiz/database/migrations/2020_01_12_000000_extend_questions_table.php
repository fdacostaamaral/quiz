<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendQuestionsTable extends Migration
{
    /**
     * Run the migration
     * 
     * @return void
     */

     public function up()
     {
         Schema::table('questions', function(Blueprint $table) {
            $table->enum('questionType', ['SingleChoice', 'MultipleChoice'])->default('MultipleChoice'); 
         });
     }

     /**
      * Reverse the migration 
      *
      * @return void
      */
     public function down()
     {
         Schema::table('questions', function(Blueprint $table) {
            $table->dropColumn('questionType');
         });
     }
      
}