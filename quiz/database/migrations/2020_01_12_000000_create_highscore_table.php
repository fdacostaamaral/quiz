<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateHighscoreTable extends Migration
{
    /**
     * Run the migration
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('highscores', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('username');
            $table->unsignedBigInteger('score');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });     
    }

    /**
     * Reverse the migration
     * 
     * @return void
     */

     public function down()
     {
        Schema::dropIfExists('highscores');
     }
}