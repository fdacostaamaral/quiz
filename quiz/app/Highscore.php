<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Highscore extends Model
{
    protected $fillable = [
        'user_id',
        'username',
        'score',
    ];

    public function scopeOfHighscore(Builder $query){
        return $query->orderByDesc('score')->groupBy('user_id')->get(['user_id', 'username', 'score']);
    }

    public function users()
    {
        return $this-belongsTo(User::class);
    }
}