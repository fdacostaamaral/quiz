<?php


namespace App\Http\Controllers;

use App\Highscore;
use Illuminate\Http\Request;

class HighscoreController extends Controller
{
    /**
     * HighscoreController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    /**
     * Display a listing of the resource
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $highscores = Highscore::all()->orderBy('score');
        return response($highscores);
    }

    /**
     *Store a newly created resource in storage 
     * 
     * @param \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        $request ->validate([
            'user_id' => 'required',
            'username' => 'required',
            'score' => 'required'
        ]);

        $highscore = new Highscore([
            'user_id' => $request->get('user_id'),
            'username' => $request->get('username'),
            'score' => $request->get('score')
        ]);

        $highscore->save();
        return response($highscore);
    }

    /**
     * Display the specified resource
     * 
     * @param \App\Highscore $highscore
     * @return \Illuminate\Http\Response
     */
    public function show(Highscore $highscore)
    {
        return response($highscore);    
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\Highscore $highscore
     * @return \Illuminate\Http\Response
     */
    public function update(Request $reqeust, Highscore $highscore)
    {
        $highscore->fill($request->all());
        $highscore->save();
    
        return response(null, 204);
    }

    /**
     * Remove the specified resource from storage
     * 
     * @param \App\Highscore $highscore
     * @return \Illuminate\Http\Response
     */
    public function destroy(Highscore $highscore)
    {
        $highscore->delete();
        return response(null, 204);
    }
}