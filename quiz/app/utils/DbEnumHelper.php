<?php

namespace App\Utils;

class DbEnumHelper {
    public static function getPossibleEnumValues($table, $column) {
        $enumString = DB::select(DB::raw('SHOW COLUMNS FROM '. $table . ' WHERE Field = "'. $column . '"'))[0]->Type;
        preg_match_all("/'([^']+)'/", $enumString, $matches);
        return isset($matches[1]) ? $matches[1] : [];
    }
}